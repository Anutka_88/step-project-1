let tab = function () {
    let tabsTitle = document.querySelectorAll('.service-item');
    let tabContent = document.querySelectorAll('.item-tab');
    let tabName;
    
    tabsTitle.forEach(item => {
    item.addEventListener('click', selectTabsTitle)
    });
    function selectTabsTitle() {
    tabsTitle.forEach(item => {
    item.classList.remove('active-tab');
    });
    this.classList.add('active-tab');
    tabName = this.getAttribute('data-tab-name');
    selectTabContent(tabName);
    }
    function selectTabContent(tabName) {
    tabContent.forEach(item => {
    item.classList.contains(tabName) ? item.classList.add('active-item') : item.classList.remove('active-item');
    })
    }
    };
    tab();

    // let pic = function () {
    //     let picTitle = document.querySelectorAll('.work-item');
    //     let picContent = document.querySelectorAll('.work-images');
    //     let picName;
        
    //     picTitle.forEach(item => {
    //     item.addEventListener('click', selectPicTitle)
    //     });
    //     function selectPicTitle() {
    //     picTitle.forEach(item => {
    //     item.classList.remove('active');
    //     });
    //     this.classList.add('active');
    //     picName = this.getAttribute('data-tab-name');
    //     selectPicContent(picName);
    //     }
    //     function selectPicContent(picName) {
    //     picContent.forEach(item => {
    //     item.classList.contains(picName) ? item.classList.add('active') : item.classList.remove('active');
    //     })
    //     }
    //     };
    //     pic();

//       const showMore = document.querySelector(".show-more");
//       let items = 0;
//       const productsLength = document.querySelectorAll(".work-images-hidden").length;
//       showMore.addEventListener('click', () => {
      
// items += 12;
// const array = Array.from(document.querySelector(".work-wrapper").children);
// const visItems = array.slice(0, items);
// visItems.forEach(el => el.classList.add("is-visible"));
// if (document.querySelectorAll(".work-images-hidden").length === 12) {
//   showMore.style.display = "none";
// }
//       });
 

    //   let offset = 0;
    //   const sliderLine = document.querySelector('.feedback-line');
      
    //   document.querySelector('.next').addEventListener('click', function(){
    //       offset = offset + 1160;
    //       if (offset > 4640) {
    //           offset = 0;
    //       }
    //       sliderLine.style.left = -offset + 'px';
    //   });
      
    //   document.querySelector('.prev').addEventListener('click', function () {
    //       offset = offset - 1160;
    //       if (offset < 0) {
    //           offset = 4640;
    //       }
    //       sliderLine.style.left = -offset + 'px';
    //   });

    const filterCards = document.querySelectorAll(".portfolio-gallery-item");
const portfolioList = document.querySelector(".portfolio-list");
const loadMoreBtn = document.querySelector(".load-btn");
const loader = document.querySelector(".loader");
let currFilter = "all";
let cardsNum = 12;

function showLoader() {
    loadMoreBtn.classList.add("hide");
    loader.classList.remove("hide");
}
function hideLoader() {
    loadMoreBtn.classList.remove("hide");
    loader.classList.add("hide");
}

showCards();

loadMoreBtn.addEventListener("click", (event) => {
    event.preventDefault();

    showLoader();
    setTimeout(() => {
        cardsNum += 12;
        hideLoader();
        showCards(currFilter);
    }, 1000);
});

portfolioList.addEventListener("click", function (event) {
    let currentBtn = document.querySelectorAll(".portfolio-item-name");
    currentBtn.forEach((item) => item.classList.remove('active'))

    event.target.classList.add("active");


    let filterBtns = event.target.dataset.filter;
    cardsNum = 12;
    currFilter = filterBtns;
    showCards(filterBtns);
});

function showCards(filter = "all") {
    let j = 0;

    for (let i = 0; i < filterCards.length; i++) {
        if (
            (filterCards[i].classList.contains(filter) || filter == "all") &&
            j < cardsNum
        ) {
            j++;
            filterCards[i].classList.remove("hide");
        } else {
            filterCards[i].classList.add("hide");
        }
    }

    if (j < cardsNum) {
        loadMoreBtn.classList.add("hide");
    } else {
        loadMoreBtn.classList.remove("hide");
    }
}
$(function () {
    $('.feedback-text').hide().first().show();
    $('.feedback-client').hide().first().show();
    $('.client-pic').hide().first().show();
    
    
    $('.slider-pic').click(function () {
        $(this).addClass('slider-pic-active').siblings().removeClass('slider-pic-active');
        switchContent();
    })
    
    $('.slider-arrow.next').click(function () {
        let activeIndex = $('.slider-pic.slider-pic-active').index() - 1;
        let newIndex = (activeIndex == 3) ? 0 : activeIndex + 1;
        $(`.slider-pic:eq(${newIndex})`).addClass('slider-pic-active').siblings().removeClass('slider-pic-active');
        switchContent();
    })
    
    $('.slider-arrow.prev').click(function () {
        let activeIndex = $('.slider-pic.slider-pic-active').index() - 1;
        let newIndex = (activeIndex == 0) ? 3 : activeIndex - 1;
        $(`.slider-pic:eq(${newIndex})`).addClass('slider-pic-active').siblings().removeClass('slider-pic-active');
        switchContent();
    })
    
    function switchContent() {
        let activeIndex = $('.slider-pic.slider-pic-active').index() - 1;
        $(`.feedback-text:eq(${activeIndex})`).show().siblings('.feedback-text').hide();
        $(`.feedback-client:eq(${activeIndex})`).show().siblings('.feedback-client').hide();
        $(`.client-pic:eq(${activeIndex})`).show().siblings('.client-pic').hide();
    }
});